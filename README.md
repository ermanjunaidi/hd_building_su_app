# HD Video Series - Building A Super User App!

# About
I get a lot of questions about how to compile Android and how to make custom kernels, so I thought I would put together a video series with everything I know. (Don't worry, that will not take long!) Sometimes, when you make a custom rom or kernel, you need an app to run a command that has root permission.

# Who is this course for?
This course is for those who are able to flash custom recoveries, like TRWP or CWM, and who can root their phones or flash custom roms. Now that you can do those things, you are ready to start building your very own custom roms and kernels!

# What do we cover?
Here we cover the bare minimum basics of building an app for your phone or tablet, using Android Studio. I walk through downloading Android Studio, setting up a Virtual Device, plugging in your real phone, and creating a very simple application that runs one command with elevated priveledges. Note that the phone must be rooted for this to work properly.

